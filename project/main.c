
/* Name: main.c
 * Project: V-USB MIDI device on Low-Speed USB
 * Author: Martin Homuth-Rosemann
 * Creation Date: 2008-03-11
 * Copyright: (c) 2008 by Martin Homuth-Rosemann.
 * License: GPL.
 *
 */

#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>

#include "usbdrv.h"
#include "oddebug.h"
#include "usbdescriptor.h"

#if DEBUG_LEVEL > 0
#	warning "Never compile production devices with debugging enabled"
#endif

#define SHPORT PORTB
#define SCK PB2
#define RCK PB1
#define S_RED PB0
#define S_GREEN PB4
#define S_BLUE PB5

#define PLAY_RED 255
#define PLAY_GREEN 255
#define PLAY_BLUE 0

#define ON_RED 255
#define ON_GREEN 0
#define ON_BLUE 255

#define SELECT_RED 0
#define SELECT_GREEN 0
#define SELECT_BLUE 0


uint8_t *red, *green, *blue, *pok;
uint8_t button_state[32], buttons_old[8];
  //0 off
  //255 on
  //1 play
  //2 selected

static uchar sendEmptyFrame;
volatile uint8_t btn_col;

uchar usbFunctionDescriptor(usbRequest_t *);
uchar usbFunctionSetup(uchar*);
uchar usbFunctionRead(uchar * , uchar);
uchar usbFunctionWrite(uchar * , uchar);
void usbFunctionWriteOut(uchar * , uchar);
static void hardwareInit(void);
void demo_matrix(uint8_t*,uint8_t*,uint8_t*,uint8_t*);
void check_buttons(void);
void set_color(uint8_t,uint8_t);
void rainbow(uint8_t);

int main(void)
{
  uint8_t i,j,y,led_gnd;
  int8_t x;

  red=malloc(32 * sizeof(uint8_t));
  green=malloc(32 * sizeof(uint8_t));
  blue=malloc(32 * sizeof(uint8_t));
  pok=malloc(32 * sizeof(uint8_t));
  
  demo_matrix(red, green, blue, pok);
  //red[31]=1;
  
  wdt_enable(WDTO_1S);
  hardwareInit();
  odDebugInit();
  usbInit();
  
  sei();


	//PINs konfiguriern
	DDRB  |= (1 << DDB0) | (1 << DDB1) | (1 << DDB2) | (1 << DDB3) | (1 << DDB4) | (1 << DDB5);
	PORTB |= (1 << PB3);//pwm
	//pb3 - pwm; pb1 - SR out clk;pb2 - SR input;pb5 - SR shift clk
	
	DDRC  |= (1 << DDC0) | (1 << DDC1) | (1 << DDC2) | (1 << DDC3);
	//led anodes
	
	DDRD |= (1 << DDD0);
	DDRD &= ~((1 << DDD4)|(1 << DDD5)|(1 << DDD6)|(1 << DDD7));
	PORTD &= ~((1 << PD4) | (1 << PD5) | (1 << PD6) | (1 << PD7));//no pull-up
	
	TCCR2A = (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);
	TCCR2B = (1 << CS20); // NO PRESCALER
	OCR2A=180;//hellikeit    


  btn_col=7;
  for (;;)
    {	
      wdt_reset();
      usbPoll();
      
      for(i=0; i<255; i++)
	{
	  led_gnd = 1;
	  
	  for(y=0;y<4;y++)
	    {


	      //uint8_t coloroffset=0;
	      check_buttons();  
	      for(x=7; x>=0; x--)
		{
		  j=4*x+y;
		  set_color(i,j);

		  //		  if(i==254)
		  //rainbow(j);
		  
		  PORTD &= ~(1<<PD0); //set button col zero		      
		  if(x-1==btn_col)
		    PORTD |= (1<<PD0);//button col one

		}
	      
	      //set color line	      
	      SHPORT |= (1 << RCK);
	      SHPORT &= ~(1 << RCK);

	      //mux led rows
	      PORTC &= 0xF0;
	      PORTC |= led_gnd;
	      led_gnd<<=1;
	    }
	}
    }
  return 0;
}

ISR(PCINT2_vect)
{
  //buttons_new[btn_col]=PIND&0xF0;
}

void check_buttons(void)
{
  uint8_t buttons_new, btn_buff, code;

  uchar on[4],off[4];
  
  on[0]=0x09;
  on[1]=0x90;
  on[3]=0x7f;
  
  off[0]=0x08;
  off[1]=0x80;
  off[3]=0x00;

  buttons_new=PIND&0xF0;
  if(btn_buff=(buttons_old[btn_col]^buttons_new))
    {

      buttons_old[btn_col]=buttons_new;
      switch(btn_buff)
	{
	case 128:
	  code=4*btn_col+3;
	  break;
	case 64:
	  code=4*btn_col+2;
	  break;
	case 32:
	  code=4*btn_col+1;
	  break;
	case 16:
	  code=4*btn_col;
	  break;
	}
      
      if(btn_buff&buttons_new)//press
	{
	  
	  
	  switch (button_state[code])
	    {
	    case 1: //play -> on
	      button_state[code]=255;
	      red[code]=ON_RED;
	      green[code]=ON_GREEN;
	      blue[code]=ON_BLUE;
	      off[2]=code+1;
	      if(usbInterruptIsReady())
		{
		  usbSetInterrupt(off, 4);
		}
	      break;
	    case 2: 
	      off[2]=code+1;
	      red[code]=SELECT_RED;
	      green[code]=SELECT_GREEN;
	      blue[code]=SELECT_BLUE;
	      if(usbInterruptIsReady())
		{
		  usbSetInterrupt(off, 4);
		}
	      break;
	      
	    default: //on -> play
	      button_state[code]=1;//255;//1
	      red[code]=PLAY_RED;
	      green[code]=PLAY_GREEN;
	      blue[code]=PLAY_BLUE;
	      on[2]=code+1;
	      if(usbInterruptIsReady())
		{
		  usbSetInterrupt(on, 4);
		}
	      break;
	    }
	  
	}
      else //release
	{
	  /*
	    if(usbInterruptIsReady())
	    {	    
	    iii = 0;
	    usbSetInterrupt(midiMsg, iii);
	    }
	      */
	}
    }
  if(btn_col>=7)
    btn_col=0;
  else
  btn_col++;  
	      

  }

void rainbow(uint8_t j)
{
  uint8_t speed=250;
  switch(pok[j])
    {
    case 0:
      green[j]+=speed;
      if(green[j]>=255-speed)
	pok[j]=1;
      break;
    case 1:
      red[j]-=speed;
      if(red[j]<=speed)
	pok[j]=2;
      break;
    case 2:
      blue[j]+=speed;
      if(blue[j]>=255-speed)
	pok[j]=3;
      break;
    case 3:
      green[j]-=speed;
      if(green[j]<=speed)
	pok[j]=4;
      break;
    case 4:
      red[j]+=speed;
      if(red[j]>=255-speed)
	pok[j]=5;
      break;
    case 5:
      blue[j]-=speed;
      if(blue[j]<=speed)
	pok[j]=0;
      break;
    }
}

void set_color(uint8_t i,uint8_t j)
{
  uint8_t buff, c, pin;
  SHPORT &= ~((1<<S_RED)|(1<<S_GREEN)|(1<<S_BLUE)); //set all colors zero

  /*
    if(red[j]&&!(i%red[j]))
    PORTB |= (1<<PB0);
    
    if(green[j]&&!(i%green[j]))
    PORTB |= (1<<PB4);
    
    if(blue[j]&&!(i%blue[j]))
    PORTB |= (1<<PB5);		  
  */


  for(c=0;c<3;c++)
    {
      switch(c)
	{
	case 0:
	  buff=red[j];
	  pin=S_RED;
	  break;
	case 1:
	  buff=green[j];
	  pin=S_GREEN;
	  break;
	case 2:
	  buff=blue[j];
	  pin=S_BLUE;
	  break;
	}
      if(buff&0x80)
	{//msb gesetzt ... >127 ... ausschalten
	  buff&=0x7F;
	  buff+=2;
	  if(buff&&(i%buff))
	    SHPORT |= (1<<pin);
	}
      else
	{//msb nicht gesetzt <127 ... einschalten
	  if(buff!=0)
	    {
	      buff^=0x7F;
	      buff+=2;
	      if(buff&&!(i%buff))
		SHPORT |= (1<<pin);
	    }
	}
    }      
		  //clk
  SHPORT |= (1 << SCK);
  SHPORT &= ~(1 << SCK);

}
/*---------------------------------------------------------------------------*/
/* hardwareInit                                                              */
/*---------------------------------------------------------------------------*/


static void hardwareInit(void)
{
	uchar i, j;
	
	
	/* activate pull-ups except on USB lines */
	USB_CFG_IOPORT =
	    (uchar) ~ ((1 << USB_CFG_DMINUS_BIT) |
		       (1 << USB_CFG_DPLUS_BIT));
	/* all pins input except USB (-> USB reset) */
#ifdef USB_CFG_PULLUP_IOPORT	/* use usbDeviceConnect()/usbDeviceDisconnect() if available */
	USBDDR = 0;		/* we do RESET by deactivating pullup */
	usbDeviceDisconnect();
#else
	USBDDR = (1 << USB_CFG_DMINUS_BIT) | (1 << USB_CFG_DPLUS_BIT);
#endif

	j = 0;
	while (--j) {		/* USB Reset by device only required on Watchdog Reset */
		i = 0;
		while (--i);	/* delay >10ms for USB reset */
	}
#ifdef USB_CFG_PULLUP_IOPORT
	usbDeviceConnect();
#else
	USBDDR = 0;		/*  remove USB reset condition */
#endif


  //  PCICR |= (1<<PCIE2);//enables external Interrupt2 at PCINT23..16
  //PCICR &= ~(1<<PCIE2);//disables external Interrupt2 at PCINT23..16  
  //PCMSK2 |= (1<<PCINT20) | (1<<PCINT21) | (1<<PCINT22) | (1<<PCINT23);//enables pin D4..7 for extenal interrupt2

}

uchar usbFunctionDescriptor(usbRequest_t * rq)
{

	if (rq->wValue.bytes[1] == USBDESCR_DEVICE) {
		usbMsgPtr = (uchar *) deviceDescrMIDI;
		return sizeof(deviceDescrMIDI);
	} else {		/* must be config descriptor */
		usbMsgPtr = (uchar *) configDescrMIDI;
		return sizeof(configDescrMIDI);
	}
}



/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

uchar usbFunctionSetup(uchar data[8])
{
	usbRequest_t *rq = (void *) data;

	// DEBUG LED
	//red[31]^=0x01;
	//LED_GND ^= 0x01;

	if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {	/* class request type */

		/*  Prepare bulk-in endpoint to respond to early termination   */
		if ((rq->bmRequestType & USBRQ_DIR_MASK) ==
		    USBRQ_DIR_HOST_TO_DEVICE)
			sendEmptyFrame = 1;
	}

	return 0xff;
}


/*---------------------------------------------------------------------------*/
/* usbFunctionRead                                                           */
/*---------------------------------------------------------------------------*/

uchar usbFunctionRead(uchar * data, uchar len)
{
	// DEBUG LED
  //red[30]^=0x01;
	//LED_GND ^= 0x02;

	data[0] = 0;
	data[1] = 0;
	data[2] = 0;
	data[3] = 0;
	data[4] = 0;
	data[5] = 0;
	data[6] = 0;

	return 7;
}


/*---------------------------------------------------------------------------*/
/* usbFunctionWrite                                                          */
/*---------------------------------------------------------------------------*/

uchar usbFunctionWrite(uchar * data, uchar len)
{
	// DEBUG LED
	//red[29]^=0x01;	
  //LED_GND ^= 0x04;
	return 1;
}


/*---------------------------------------------------------------------------*/
/* usbFunctionWriteOut                                                       */
/*                                                                           */
/* this Function is called if a MIDI Out message (from PC) arrives.          */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void usbFunctionWriteOut(uchar * data, uchar len)
{
	// DEBUG LED
	//red[28]^=0x01;
	//LED_GND ^= 0x20;
}


/* Simple monophonic keyboard
   The following function returns a midi note value for the first key pressed. 
   Key 0 -> 60 (middle C),
   Key 1 -> 62 (D)
   Key 2 -> 64 (E)
   Key 3 -> 65 (F)
   Key 4 -> 67 (G)
   Key 5 -> 69 (A)
   Key 6 -> 71 (B)
   Key 7 -> 72 (C)
 * returns 0 if no key is pressed.
 */

void demo_matrix(uint8_t* red, uint8_t* green, uint8_t* blue, uint8_t* pok)
{
  uint8_t y;
  //uint8_t c=0,mode=0;

  for(y=0;y<32;y++)
    {
    /*            
      switch(mode)
	{
	case 0:
	  red[y]=255;
	  green[y]=255-c*46;
	  blue[y]=0;
	  pok[y]=0;
	  c++;
	  if(c*46>=211)
	    {
	      mode=1;
	      c=0;
	    }
	  break;
	case 1:
	  red[y]=255-c*46;
	  green[y]=255;
	  blue[y]=0;
	  pok[y]=1;
	  c++;
	  if(c*46>=211)
	    {
	      mode=2;
	      c=0;
	    }
	  break;
	case 2:
	  blue[y]=c*46;
	  red[y]=0;
	  green[y]=255;
	  pok[y]=2;
	  c++;
	  if(c*46>=211)
	    {
	      mode=3;
	      c=0;
	    }
	  break;
	case 3:
	  blue[y]=255;
	  red[y]=0;
	  green[y]=255-c*46;
	  pok[y]=3;
	  c++;
	  if(c*46>=211)
	    {
	      mode=4;
	      c=0;
	    }
	  break;
	case 4:
	  green[y]=0;
	  blue[y]=255;
	  red[y]=c*46;
	  pok[y]=4;
	  c++;
	  if(c*46>=211)
	    {
	      mode=5;
	      c=0;
	    }
	  break;
	case 5:
	  green[y]=0;
	  blue[y]=255-c*46;
	  red[y]=255;
	  pok[y]=5;
	  c++;
	  if(c*46>=211)
	    {
	      mode=0;
	      c=0;
	    }
	  break;
	}
  *//*      
            
      red[y]=0;
      green[y]=0;
      blue[y]=255;*/
      button_state[y]=0;
      
    }
for(y=0;y<4;y++)
  {
    red[y]=255;
    green[y]=255;
    blue[y]=255;
    pok[y]=6;
  }
for(y=4;y<8;y++)
  {
    red[y]=255;
    green[y]=0;
    blue[y]=0;
    pok[y]=0;
  }
for(y=8;y<12;y++)
  {
    red[y]=255;
    green[y]=255;
    blue[y]=0;
    pok[y]=1;
  }
for(y=12;y<16;y++)
  {
    red[y]=0;
    green[y]=255;
    blue[y]=0;
    pok[y]=2;
  }
for(y=16;y<20;y++)
  {
    red[y]=0;
    green[y]=255;
    blue[y]=255;
    pok[y]=3;
  }
for(y=20;y<24;y++)
  {
    red[y]=0;
    green[y]=0;
    blue[y]=255;
    pok[y]=4;
  }
for(y=24;y<28;y++)
  {
    red[y]=255;
    green[y]=0;
    blue[y]=255;
    pok[y]=5;
  }
for(y=28;y<32;y++)
  {
    red[y]=255;
    green[y]=255;
    blue[y]=255;
    pok[y]=6;
  }
}
