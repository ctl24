#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <inttypes.h>

uint8_t *red, *green, *blue;

void init_timer(void);
void demo_matrix(uint8_t*, uint8_t*, uint8_t*);
void display_matrix(void);

int main(void)
{

  red=malloc(32 * sizeof(uint8_t));
  green=malloc(32 * sizeof(uint8_t));
  blue=malloc(32 * sizeof(uint8_t));

  init_timer();
  demo_matrix(red, green, blue);

  //  char s[4];  
  
  //PINs konfiguriern
  DDRB  = (1 << DDB1) | (1 << DDB2) | (1 << DDB3) | (1 << DDB4) | (1 << DDB5);
  PORTB = (1 << PB3);//pwm
  //pb3 - pwm; pb1 - SR out clk;pb2 - SR input;pb4 - SR reset;pb5 - SR shift clk

  DDRC  = (1 << DDC0) | (1 << DDC1) | (1 << DDC2) | (1 << DDC3);
    //led anodes
  
  DDRD  = (1 << DDD0) | (1 << DDD5) | (1 << DDD6) | (1 << DDD7) ;


  PORTB |= (1 << PB4); //reset
  OCR2A=0;//hellikeit    
  red[31]=1;

  while(1)
    {
      display_matrix();

    }
  return 0; 
}


void init_timer(void)
{

  //Timer setzen, PWM
  //Timer0 PinD6 OC0A
  //---TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00); // FAST PWM ON OC0A OC0B
  //---TCCR0B = (1 << CS00); // NO PRESCALER
  //TIMSK0 = (1 << OCIE0A);//interupt enable

  //OC0A green
  //OC0B red
  //OC2A blue

  //Timer2 -- helligkeit
  TCCR2A = (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);
  TCCR2B = (1 << CS20); // NO PRESCALER
  //TIMSK0 = (1 << OCIE0A);//interupt enable

}

void demo_matrix(uint8_t* red, uint8_t* green, uint8_t* blue)
{
  for(int y=0;y<32;y++)
    {
      red[y]=0;
      green[y]=0;
      blue[y]=0;
    }
}
/*

void demo_matrix(uint8_t* red, uint8_t* green, uint8_t* blue)
{
  for(int y=0;y<32;y++)
    {
	red[y]=y*8;
	green[y]=255-y*8;
	blue[y]=255-y*8;
    }
}
 */

void display_matrix(void)
{
  uint8_t i,j,x,y,portc,color;

      for(i=0; i<255; i++)
	{
	  portc = 1;
	  
	  for(y=0;y<32;y+=8)
	    {
	      
	      for(x =0; x<8; x++)
		{
		  PORTD &= ~((1<<PD5)|(1<<PD6)|(1<<PD7));
		  j=x+y;
		  
		  if(red[j]&&!(i%red[j]))
		    PORTD |= (1<<PD5);
		  
		  if(green[j]&&!(i%green[j]))
		    PORTD |= (1<<PD6);
		  
		  if(blue[j]&&!(i%blue[j]))
		  PORTD |= (1<<PD7);		  

		  //shift
		  PORTB |= (1 << PB5);
		  PORTB &= ~ (1 << PB5);
		}

	      //set line
	      PORTB |= (1 << PB1);
	      PORTB &= ~(1 << PB1);

	      PORTC |= 0x0F;
	      PORTC &= ~portc;
	      portc<<=1;

	    }
	}
}

/*
- timer interupt
+ y vor x
- color map
- shift reg pwm!!!overkill 4x more lightness
 */
